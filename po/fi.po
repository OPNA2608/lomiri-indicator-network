# Finnish translation for lomiri-indicator-network
# Copyright (c) 2014 Rosetta Contributors and Canonical Ltd 2014
# This file is distributed under the same license as the lomiri-indicator-network package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2014.
#
msgid ""
msgstr ""
"Project-Id-Version: lomiri-indicator-network\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-04-25 18:01+0000\n"
"PO-Revision-Date: 2025-01-04 11:00+0000\n"
"Last-Translator: Jiri Grönroos <jiri.gronroos@iki.fi>\n"
"Language-Team: Finnish <https://hosted.weblate.org/projects/lomiri/"
"lomiri-indicator-network/fi/>\n"
"Language: fi\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 5.10-dev\n"
"X-Launchpad-Export-Date: 2016-10-21 07:05+0000\n"

#: doc/qt/qml/examples/example_networking_status.qml:46
msgid "Networking Status"
msgstr "Verkon tila"

#: src/agent/SecretRequest.cpp:61
#, qt-format
msgid "Connect to “%1”"
msgstr "Yhdistä verkkoon “%1”"

#: src/agent/SecretRequest.cpp:66
msgid "WPA"
msgstr "WPA"

#: src/agent/SecretRequest.cpp:68
msgid "WEP"
msgstr "WEP"

#: src/agent/SecretRequest.cpp:74
msgid "Connect"
msgstr "Yhdistä"

#: src/agent/SecretRequest.cpp:75 src/vpn-editor/DialogFile/DialogFile.qml:156
msgid "Cancel"
msgstr "Peru"

#: src/indicator/factory.cpp:187
msgid "Wi-Fi"
msgstr "Wifi"

#: src/indicator/factory.cpp:198
msgid "Flight Mode"
msgstr "Lentotila"

#: src/indicator/factory.cpp:208
msgid "Cellular data"
msgstr "Mobiilidata"

#: src/indicator/factory.cpp:221
msgid "Hotspot"
msgstr "Yhteyspiste"

#: src/indicator/menuitems/wifi-link-item.cpp:109
msgid "Other network…"
msgstr "Muu verkko…"

#: src/indicator/menuitems/wwan-link-item.cpp:90
msgid "No SIM"
msgstr "Ei SIM-korttia"

#: src/indicator/menuitems/wwan-link-item.cpp:97
msgid "SIM Error"
msgstr "SIM-virhe"

#: src/indicator/menuitems/wwan-link-item.cpp:105
msgid "SIM Locked"
msgstr "SIM lukittu"

#: src/indicator/menuitems/wwan-link-item.cpp:118
msgid "Unregistered"
msgstr "Rekisteröitymätön"

#: src/indicator/menuitems/wwan-link-item.cpp:123
#: src/indicator/nmofono/hotspot-manager.cpp:111
msgid "Unknown"
msgstr "Tuntematon"

#: src/indicator/menuitems/wwan-link-item.cpp:128
msgid "Denied"
msgstr "Estetty"

#: src/indicator/menuitems/wwan-link-item.cpp:133
msgid "Searching"
msgstr "Etsitään"

#: src/indicator/menuitems/wwan-link-item.cpp:145
msgid "No Signal"
msgstr "Ei signaalia"

#: src/indicator/menuitems/wwan-link-item.cpp:157
#: src/indicator/menuitems/wwan-link-item.cpp:164
msgid "Offline"
msgstr "Ei paikalla"

#: src/indicator/nmofono/hotspot-manager.cpp:51
msgid "Unknown error"
msgstr "Tuntematon virhe"

#: src/indicator/nmofono/hotspot-manager.cpp:53
msgid "No reason given"
msgstr "Syytä ei annettu"

#: src/indicator/nmofono/hotspot-manager.cpp:55
msgid "Device is now managed"
msgstr "Laite on nyt hallinnassa"

#: src/indicator/nmofono/hotspot-manager.cpp:57
msgid "Device is now unmanaged"
msgstr "Laite ei ole nyt hallinnassa"

#: src/indicator/nmofono/hotspot-manager.cpp:59
msgid "The device could not be readied for configuration"
msgstr "Laitteen valmistelu määrityksiä varten epäonnistui"

#: src/indicator/nmofono/hotspot-manager.cpp:61
msgid ""
"IP configuration could not be reserved (no available address, timeout, etc.)"
msgstr ""
"IP-määritysten tallentaminen epäonnistui (ei osoitetta saatavilla, "
"aikakatkaisu tms.)"

#: src/indicator/nmofono/hotspot-manager.cpp:63
msgid "The IP configuration is no longer valid"
msgstr "IP-määritykset eivät ole enää kelvollisia"

#: src/indicator/nmofono/hotspot-manager.cpp:65
msgid "Your authentication details were incorrect"
msgstr "Tunnistautumistietosi olivat väärin"

#: src/indicator/nmofono/hotspot-manager.cpp:67
msgid "802.1X supplicant disconnected"
msgstr "802.1X-anoja katkaisi yhteyden"

#: src/indicator/nmofono/hotspot-manager.cpp:69
msgid "802.1X supplicant configuration failed"
msgstr "802.1X-anojan asetukset epäonnistuivat"

#: src/indicator/nmofono/hotspot-manager.cpp:71
msgid "802.1X supplicant failed"
msgstr "802.1X-anoja epäonnistui"

#: src/indicator/nmofono/hotspot-manager.cpp:73
msgid "802.1X supplicant took too long to authenticate"
msgstr "802.1X-anojan tunnistautuminen kesti liian kauan"

#: src/indicator/nmofono/hotspot-manager.cpp:75
msgid "DHCP client failed to start"
msgstr "DHCP-asiakkaan käynnistys epäonnistui"

#: src/indicator/nmofono/hotspot-manager.cpp:77
msgid "DHCP client error"
msgstr "DHCP-asiakkaan virhe"

#: src/indicator/nmofono/hotspot-manager.cpp:79
msgid "DHCP client failed"
msgstr "DHCP-asiakas epäonnistui"

#: src/indicator/nmofono/hotspot-manager.cpp:81
msgid "Shared connection service failed to start"
msgstr "Jaetun yhteyden palvelun käynnistys epäonnistui"

#: src/indicator/nmofono/hotspot-manager.cpp:83
msgid "Shared connection service failed"
msgstr "Jaetun yhteyden palvelu epäonnistui"

#: src/indicator/nmofono/hotspot-manager.cpp:85
msgid "Necessary firmware for the device may be missing"
msgstr "Laitteelle vaadittava laiteohjelmisto saattaa puuttua"

#: src/indicator/nmofono/hotspot-manager.cpp:87
msgid "The device was removed"
msgstr "Laite irrotettiin"

#: src/indicator/nmofono/hotspot-manager.cpp:89
msgid "NetworkManager went to sleep"
msgstr "NetworkManager meni lepotilaan"

#: src/indicator/nmofono/hotspot-manager.cpp:91
msgid "The device's active connection disappeared"
msgstr "Laitteen aktiivinen yhteys katosi"

#: src/indicator/nmofono/hotspot-manager.cpp:93
msgid "Device disconnected by user or client"
msgstr "Yhteys laitteeseen katkaistu käyttäjän tai asiakkaan toimesta"

#: src/indicator/nmofono/hotspot-manager.cpp:95
msgid "The device's existing connection was assumed"
msgstr "Oletettiin laitteen olemassa olevaa yhteyttä"

#: src/indicator/nmofono/hotspot-manager.cpp:97
msgid "The supplicant is now available"
msgstr "Anoja on nyt saatavilla"

#: src/indicator/nmofono/hotspot-manager.cpp:99
msgid "The modem could not be found"
msgstr "Modeemia ei löytynyt"

#: src/indicator/nmofono/hotspot-manager.cpp:101
msgid "The Bluetooth connection failed or timed out"
msgstr "Bluetooth-yhteys epäonnistui tai se aikakatkaistiin"

#: src/indicator/nmofono/hotspot-manager.cpp:103
msgid "A dependency of the connection failed"
msgstr "Yhteyden riippuvuus epäonnistui"

#: src/indicator/nmofono/hotspot-manager.cpp:105
msgid "ModemManager is unavailable"
msgstr "ModemManager ei ole käytettävissä"

#: src/indicator/nmofono/hotspot-manager.cpp:107
msgid "The Wi-Fi network could not be found"
msgstr "Wifi-verkkoa ei löytynyt"

#: src/indicator/nmofono/hotspot-manager.cpp:109
msgid "A secondary connection of the base connection failed"
msgstr "Perusyhteyden toissijainen yhteys epäonnistui"

#: src/indicator/nmofono/hotspot-manager.cpp:654
msgid "Failed to enable hotspot"
msgstr "Yhteyspisteen aktivointi epäonnistui"

#: src/indicator/nmofono/vpn/vpn-manager.cpp:87
#, qt-format
msgid "VPN connection %1"
msgstr "VPN-yhteys %1"

#. TRANSLATORS: this is the indicator title shown on the top header of the indicator area
#: src/indicator/root-state.cpp:314
msgid "Network"
msgstr "Verkko"

#: src/indicator/sections/vpn-section.cpp:147
msgid "VPN settings…"
msgstr "VPN-asetukset…"

#: src/indicator/sections/wifi-section.cpp:58
msgid "Wi-Fi settings…"
msgstr "Wifi-asetukset…"

#: src/indicator/sections/wwan-section.cpp:102
msgid "Cellular settings…"
msgstr "Mobiiliverkon asetukset…"

#: src/indicator/sim-unlock-dialog.cpp:144
msgid "Sorry, incorrect %{1} PIN."
msgstr "Pahoittelut, väärä %{1} PIN."

#: src/indicator/sim-unlock-dialog.cpp:149
#: src/indicator/sim-unlock-dialog.cpp:178
msgid "This will be your last attempt."
msgstr "Tämä on viimeinen yritys."

#: src/indicator/sim-unlock-dialog.cpp:151
msgid ""
"If %{1} PIN is entered incorrectly you will require your PUK code to unlock."
msgstr ""
"Jos %{1} PIN syötetään väärin, PUK-koodia tarvitaan lukituksen avaamiseen."

#: src/indicator/sim-unlock-dialog.cpp:161
msgid "Sorry, your %{1} is now blocked."
msgstr "Valitettavasti %{1} on nyt lukittu."

#: src/indicator/sim-unlock-dialog.cpp:166
msgid "Please enter your PUK code to unblock SIM card."
msgstr "Anna PUK-koodi avataksesi SIM-kortin lukituksen."

#: src/indicator/sim-unlock-dialog.cpp:168
msgid "You may need to contact your network provider for PUK code."
msgstr "Ota yhteys palveluntarjoajaan saadaksesi PUK-koodin."

#: src/indicator/sim-unlock-dialog.cpp:176
#: src/indicator/sim-unlock-dialog.cpp:190
msgid "Sorry, incorrect PUK."
msgstr "Väärä PUK-koodi."

#: src/indicator/sim-unlock-dialog.cpp:180
msgid ""
"If PUK code is entered incorrectly, your SIM card will be blocked and needs "
"replacement."
msgstr ""
"Jos annettava PUK-koodi on virheellinen, SIM-kortti lukkiutuu ja se pitää "
"vaihtaa uuteen."

#: src/indicator/sim-unlock-dialog.cpp:182
msgid "Please contact your network provider."
msgstr "Ota yhteys palveluntarjoajaan."

#: src/indicator/sim-unlock-dialog.cpp:192
msgid "Your SIM card is now permanently blocked and needs replacement."
msgstr ""
"SIM-kortti on nyt lukittu pysyvästi, joten se tulee korvata uudella SIM-"
"kortilla."

#: src/indicator/sim-unlock-dialog.cpp:194
msgid "Please contact your service provider."
msgstr "Ole yhteydessä palveluntarjoajaasi."

#: src/indicator/sim-unlock-dialog.cpp:218
msgid "Sorry, incorrect PIN"
msgstr "Väärä PIN-koodi"

#: src/indicator/sim-unlock-dialog.cpp:230
msgid "Sorry, incorrect PUK"
msgstr "Väärä PUK-koodi"

#: src/indicator/sim-unlock-dialog.cpp:292
msgid "Enter %{1} PIN"
msgstr "Syötä %{1} PIN"

#: src/indicator/sim-unlock-dialog.cpp:300
msgid "Enter PUK code"
msgstr "Anna PUK-koodi"

#: src/indicator/sim-unlock-dialog.cpp:304
msgid "Enter PUK code for %{1}"
msgstr "Syötä PUK-koodi – %{1}"

#: src/indicator/sim-unlock-dialog.cpp:315
#, c-format
msgid "1 attempt remaining"
msgid_plural "%d attempts remaining"
msgstr[0] "1 yritys jäljellä"
msgstr[1] "%d yritystä jäljellä"

#: src/indicator/sim-unlock-dialog.cpp:326
msgid "Enter new %{1} PIN"
msgstr "Syötä uusi %{1} PIN"

#: src/indicator/sim-unlock-dialog.cpp:334
msgid "Confirm new %{1} PIN"
msgstr "Vahvista uusi %{1} PIN"

#: src/indicator/sim-unlock-dialog.cpp:373
msgid "PIN codes did not match."
msgstr "PIN-koodit eivät täsmänneet."

#: src/indicator/vpn-status-notifier.cpp:48
#, qt-format
msgid "The VPN connection '%1' failed."
msgstr "VPN-yhteys '%1' epäonnistui."

#: src/indicator/vpn-status-notifier.cpp:50
#, qt-format
msgid ""
"The VPN connection '%1' failed because the network connection was "
"interrupted."
msgstr "VPN-yhteys '%1' epäonnistui, koska verkkoyhteys keskeytettiin."

#: src/indicator/vpn-status-notifier.cpp:51
#, qt-format
msgid ""
"The VPN connection '%1' failed because the VPN service stopped unexpectedly."
msgstr "VPN-yhteys '%1' epäonnistui, koska VPN-palvelu pysähtyi odottamatta."

#: src/indicator/vpn-status-notifier.cpp:52
#, qt-format
msgid ""
"The VPN connection '%1' failed because the VPN service returned invalid "
"configuration."
msgstr ""
"VPN-yhteys '%1' epäonnistui, koska VPN-palvelu palautti virheelliset "
"määritykset."

#: src/indicator/vpn-status-notifier.cpp:53
#, qt-format
msgid ""
"The VPN connection '%1' failed because the connection attempt timed out."
msgstr "VPN-yhteys '%1' epäonnistui, koska yhteysyritys aikakatkaistiin."

#: src/indicator/vpn-status-notifier.cpp:54
#, qt-format
msgid ""
"The VPN connection '%1' failed because the VPN service did not start in time."
msgstr ""
"VPN-yhteys '%1' epäonnistui, koska VPN-palvelu ei käynnistynyt ajallaan."

#: src/indicator/vpn-status-notifier.cpp:55
#, qt-format
msgid "The VPN connection '%1' failed because the VPN service failed to start."
msgstr ""
"VPN-yhteys '%1' epäonnistui, koska VPN-palvelun käynnistyminen epäonnistui."

#: src/indicator/vpn-status-notifier.cpp:56
#, qt-format
msgid "The VPN connection '%1' failed because there were no valid VPN secrets."
msgstr ""
"VPN-yhteys '%1' epäonnistui, koska tarjolla ei ole kelvollisia VPN-"
"salaisuuksia."

#: src/indicator/vpn-status-notifier.cpp:57
#, qt-format
msgid "The VPN connection '%1' failed because of invalid VPN secrets."
msgstr "VPN-yhteys '%1' epäonnistui, koska VPN-salaisuudet ovat virheelliset."

#: src/indicator/vpn-status-notifier.cpp:68
msgid "VPN Connection Failed"
msgstr "VPN-yhteys epäonnistui"

#: src/vpn-editor/DialogFile/DialogFile.qml:164
msgid "Accept"
msgstr "Hyväksy"

#: src/vpn-editor/Openvpn/AdvancedGeneral.qml:24
#: src/vpn-editor/Openvpn/Editor.qml:113 src/vpn-editor/Pptp/Advanced.qml:24
#: src/vpn-editor/Pptp/Editor.qml:92
msgid "Advanced"
msgstr "Lisäasetukset"

#: src/vpn-editor/Openvpn/AdvancedGeneral.qml:44
#: src/vpn-editor/Pptp/Advanced.qml:44
msgid "Only use connection for VPN resources"
msgstr "Käytä yhteyttä vain VPN-resursseihin"

#: src/vpn-editor/Openvpn/AdvancedGeneral.qml:48
msgid "Use custom gateway port:"
msgstr "Käytä mukautettua yhteyskäytävän porttia:"

#: src/vpn-editor/Openvpn/AdvancedGeneral.qml:64
msgid "Use renegotiation interval:"
msgstr "Käytä uudelleenneuvottelun väliä:"

#: src/vpn-editor/Openvpn/AdvancedGeneral.qml:85
msgid "Use LZO data compression"
msgstr "Käytä LZO-datapakkausta"

#: src/vpn-editor/Openvpn/AdvancedGeneral.qml:94
msgid "Use a TCP connection"
msgstr "Käytä TCP-yhteyttä"

#: src/vpn-editor/Openvpn/AdvancedGeneral.qml:98
msgid "Use custom virtual device type:"
msgstr "Käytä mukautettua virtuaalilaitteen tyyppiä:"

#: src/vpn-editor/Openvpn/AdvancedGeneral.qml:105
msgid "TUN"
msgstr "TUN"

#: src/vpn-editor/Openvpn/AdvancedGeneral.qml:106
msgid "TAP"
msgstr "TAP"

#: src/vpn-editor/Openvpn/AdvancedGeneral.qml:117
msgid "(automatic)"
msgstr "(automaattinen)"

#: src/vpn-editor/Openvpn/AdvancedGeneral.qml:121
msgid "and name:"
msgstr "ja nimi:"

#: src/vpn-editor/Openvpn/AdvancedGeneral.qml:125
msgid "Use custom tunnel MTU:"
msgstr "Käytä mukautettua tunnelin MTU:ta:"

#: src/vpn-editor/Openvpn/AdvancedGeneral.qml:142
msgid "Use custom UDP fragment size:"
msgstr ""

#: src/vpn-editor/Openvpn/AdvancedGeneral.qml:164
msgid "Restrict tunnel TCP MSS"
msgstr ""

#: src/vpn-editor/Openvpn/AdvancedGeneral.qml:173
msgid "Randomize remote hosts"
msgstr ""

#: src/vpn-editor/Openvpn/AdvancedProxies.qml:28
#: src/vpn-editor/Openvpn/Editor.qml:132
msgid "Proxies"
msgstr "Välityspalvelimet"

#: src/vpn-editor/Openvpn/AdvancedProxies.qml:43
msgid "Proxy type:"
msgstr "Välityspalvelimen tyyppi:"

#: src/vpn-editor/Openvpn/AdvancedProxies.qml:45
msgid "Not required"
msgstr "Ei vaadittu"

#: src/vpn-editor/Openvpn/AdvancedProxies.qml:46
msgid "HTTP"
msgstr "HTTP"

#: src/vpn-editor/Openvpn/AdvancedProxies.qml:47
msgid "SOCKS"
msgstr "SOCKS"

#: src/vpn-editor/Openvpn/AdvancedProxies.qml:54
msgid "Server address:"
msgstr "Palvelimen osoite:"

#: src/vpn-editor/Openvpn/AdvancedProxies.qml:65
msgid "Port:"
msgstr "Portti:"

#: src/vpn-editor/Openvpn/AdvancedProxies.qml:77
msgid "Retry indefinitely:"
msgstr ""

#: src/vpn-editor/Openvpn/AdvancedProxies.qml:87
msgid "Proxy username:"
msgstr "Välityspalvelimen käyttäjätunnus:"

#: src/vpn-editor/Openvpn/AdvancedProxies.qml:98
msgid "Proxy password:"
msgstr "Välityspalvelimen salasana:"

#: src/vpn-editor/Openvpn/AdvancedSecurity.qml:24
#: src/vpn-editor/Openvpn/Editor.qml:119 src/vpn-editor/Pptp/Advanced.qml:97
msgid "Security"
msgstr "Tietoturva"

#: src/vpn-editor/Openvpn/AdvancedSecurity.qml:39
msgid "Cipher:"
msgstr "Algoritmi:"

#: src/vpn-editor/Openvpn/AdvancedSecurity.qml:41
#: src/vpn-editor/Openvpn/AdvancedSecurity.qml:83
msgid "Default"
msgstr "Oletus"

#: src/vpn-editor/Openvpn/AdvancedSecurity.qml:42
msgid "DES-CBC"
msgstr "DES-CBC"

#: src/vpn-editor/Openvpn/AdvancedSecurity.qml:43
msgid "RC2-CBC"
msgstr "RC2-CBC"

#: src/vpn-editor/Openvpn/AdvancedSecurity.qml:44
msgid "DES-EDE-CBC"
msgstr "DES-EDE-CBC"

#: src/vpn-editor/Openvpn/AdvancedSecurity.qml:45
msgid "DES-EDE3-CBC"
msgstr "DES-EDE3-CBC"

#: src/vpn-editor/Openvpn/AdvancedSecurity.qml:46
msgid "DESX-CBC"
msgstr "DESX-CBC"

#: src/vpn-editor/Openvpn/AdvancedSecurity.qml:47
msgid "RC2-40-CBC"
msgstr "RC2-40-CBC"

#: src/vpn-editor/Openvpn/AdvancedSecurity.qml:48
msgid "CAST5-CBC"
msgstr "CAST5-CBC"

#: src/vpn-editor/Openvpn/AdvancedSecurity.qml:49
msgid "AES-128-CBC"
msgstr "AES-128-CBC"

#: src/vpn-editor/Openvpn/AdvancedSecurity.qml:50
msgid "AES-192-CBC"
msgstr "AES-192-CBC"

#: src/vpn-editor/Openvpn/AdvancedSecurity.qml:51
msgid "AES-256-CBC"
msgstr "AES-256-CBC"

#: src/vpn-editor/Openvpn/AdvancedSecurity.qml:52
msgid "CAMELLIA-128-CBC"
msgstr "CAMELLIA-128-CBC"

#: src/vpn-editor/Openvpn/AdvancedSecurity.qml:53
msgid "CAMELLIA-192-CBC"
msgstr "CAMELLIA-192-CBC"

#: src/vpn-editor/Openvpn/AdvancedSecurity.qml:54
msgid "CAMELLIA-256-CBC"
msgstr "CAMELLIA-256-CBC"

#: src/vpn-editor/Openvpn/AdvancedSecurity.qml:55
msgid "SEED-CBC"
msgstr "SEED-CBC"

#: src/vpn-editor/Openvpn/AdvancedSecurity.qml:56
msgid "AES-128-CBC-HMAC-SHA1"
msgstr "AES-128-CBC-HMAC-SHA1"

#: src/vpn-editor/Openvpn/AdvancedSecurity.qml:57
msgid "AES-256-CBC-HMAC-SHA1"
msgstr "AES-256-CBC-HMAC-SHA1"

#: src/vpn-editor/Openvpn/AdvancedSecurity.qml:64
msgid "Use cipher key size:"
msgstr ""

#: src/vpn-editor/Openvpn/AdvancedSecurity.qml:81
msgid "HMAC authentication:"
msgstr "HMAC-todennus:"

#: src/vpn-editor/Openvpn/AdvancedSecurity.qml:84
#: src/vpn-editor/Openvpn/AdvancedTls.qml:86
#: src/vpn-editor/Openvpn/StaticKey.qml:37
msgid "None"
msgstr "Ei mitään"

#: src/vpn-editor/Openvpn/AdvancedSecurity.qml:85
msgid "RSA MD-4"
msgstr "RSA MD-4"

#: src/vpn-editor/Openvpn/AdvancedSecurity.qml:86
msgid "MD-5"
msgstr "MD-5"

#: src/vpn-editor/Openvpn/AdvancedSecurity.qml:87
msgid "SHA-1"
msgstr "SHA-1"

#: src/vpn-editor/Openvpn/AdvancedSecurity.qml:88
msgid "SHA-224"
msgstr "SHA-224"

#: src/vpn-editor/Openvpn/AdvancedSecurity.qml:89
msgid "SHA-256"
msgstr "SHA-256"

#: src/vpn-editor/Openvpn/AdvancedSecurity.qml:90
msgid "SHA-384"
msgstr "SHA-384"

#: src/vpn-editor/Openvpn/AdvancedSecurity.qml:91
msgid "SHA-512"
msgstr "SHA-512"

#: src/vpn-editor/Openvpn/AdvancedSecurity.qml:92
msgid "RIPEMD-160"
msgstr "RIPEMD-160"

#: src/vpn-editor/Openvpn/AdvancedTls.qml:25
msgid "TLS authentication:"
msgstr "TLS-todennus:"

#: src/vpn-editor/Openvpn/AdvancedTls.qml:46
msgid "Subject match:"
msgstr ""

#: src/vpn-editor/Openvpn/AdvancedTls.qml:50
msgid "Verify peer certificate:"
msgstr ""

#: src/vpn-editor/Openvpn/AdvancedTls.qml:57
msgid "Peer certificate TLS type:"
msgstr ""

#: src/vpn-editor/Openvpn/AdvancedTls.qml:59
msgid "Server"
msgstr "Palvelin"

#: src/vpn-editor/Openvpn/AdvancedTls.qml:60
msgid "Client"
msgstr "Asiakas"

#: src/vpn-editor/Openvpn/AdvancedTls.qml:68
msgid "Use additional TLS authentication:"
msgstr ""

#: src/vpn-editor/Openvpn/AdvancedTls.qml:75
msgid "Key file:"
msgstr "Avaintiedosto:"

#: src/vpn-editor/Openvpn/AdvancedTls.qml:84
#: src/vpn-editor/Openvpn/StaticKey.qml:35
msgid "Key direction:"
msgstr "Avaimen suunta:"

#: src/vpn-editor/Openvpn/AdvancedTls.qml:87
#: src/vpn-editor/Openvpn/StaticKey.qml:38
msgid "0"
msgstr "0"

#: src/vpn-editor/Openvpn/AdvancedTls.qml:88
#: src/vpn-editor/Openvpn/StaticKey.qml:39
msgid "1"
msgstr "1"

#: src/vpn-editor/Openvpn/Editor.qml:65 src/vpn-editor/Pptp/Editor.qml:35
msgid "General"
msgstr "Yleiset"

#: src/vpn-editor/Openvpn/Editor.qml:74 src/vpn-editor/Pptp/Editor.qml:44
msgid "ID:"
msgstr ""

#: src/vpn-editor/Openvpn/Editor.qml:77
msgid "Authentication"
msgstr "Todennus"

#: src/vpn-editor/Openvpn/Editor.qml:86
msgid "Remote:"
msgstr "Etä:"

#: src/vpn-editor/Openvpn/Editor.qml:91
msgid "Certificates (TLS)"
msgstr "Varmenteet (TLS)"

#: src/vpn-editor/Openvpn/Editor.qml:92
msgid "Password"
msgstr "Salasana"

#: src/vpn-editor/Openvpn/Editor.qml:93
msgid "Password with certificates (TLS)"
msgstr "Salasana ja varmenteet (TLS)"

#: src/vpn-editor/Openvpn/Editor.qml:94
msgid "Static key"
msgstr "Kiinteä avain"

#: src/vpn-editor/Openvpn/Editor.qml:98
msgid "Type:"
msgstr "Tyyppi:"

#: src/vpn-editor/Openvpn/Editor.qml:125
msgid "TLS"
msgstr "TLS"

#: src/vpn-editor/Openvpn/Password.qml:32
#: src/vpn-editor/Openvpn/PasswordTls.qml:32
msgid "Username:"
msgstr "Käyttäjätunnus:"

#: src/vpn-editor/Openvpn/Password.qml:42
#: src/vpn-editor/Openvpn/PasswordTls.qml:42 src/vpn-editor/Pptp/Editor.qml:76
msgid "Password:"
msgstr "Salasana:"

#: src/vpn-editor/Openvpn/Password.qml:51
#: src/vpn-editor/Openvpn/PasswordTls.qml:60 src/vpn-editor/Openvpn/Tls.qml:40
msgid "CA certificate:"
msgstr "CA-varmenne:"

#: src/vpn-editor/Openvpn/PasswordTls.qml:51 src/vpn-editor/Openvpn/Tls.qml:31
msgid "User certificate:"
msgstr "Käyttäjävarmenne:"

#: src/vpn-editor/Openvpn/PasswordTls.qml:69 src/vpn-editor/Openvpn/Tls.qml:49
msgid "Private key:"
msgstr "Yksityinen avain:"

#: src/vpn-editor/Openvpn/PasswordTls.qml:79 src/vpn-editor/Openvpn/Tls.qml:58
msgid "Key password:"
msgstr "Avaimen salasana:"

#: src/vpn-editor/Openvpn/StaticKey.qml:31
msgid "Static key:"
msgstr "Kiinteä avain:"

#: src/vpn-editor/Openvpn/StaticKey.qml:52
msgid "Remote IP:"
msgstr "Etä-IP:"

#: src/vpn-editor/Openvpn/StaticKey.qml:62
msgid "Local IP:"
msgstr "Paikallinen IP:"

#: src/vpn-editor/Pptp/Advanced.qml:47
msgid "Authentication methods"
msgstr "Todennustavat"

#: src/vpn-editor/Pptp/Advanced.qml:55
msgid "PAP"
msgstr "PAP"

#: src/vpn-editor/Pptp/Advanced.qml:65
msgid "CHAP"
msgstr "CHAP"

#: src/vpn-editor/Pptp/Advanced.qml:75
msgid "MSCHAP"
msgstr "MSCHAP"

#: src/vpn-editor/Pptp/Advanced.qml:84
msgid "MSCHAPv2"
msgstr "MSCHAPv2"

#: src/vpn-editor/Pptp/Advanced.qml:93
msgid "EAP"
msgstr "EAP"

#: src/vpn-editor/Pptp/Advanced.qml:105
msgid "Use Point-to-Point encryption"
msgstr "Käytä päästä päähän -salausta"

#: src/vpn-editor/Pptp/Advanced.qml:110
msgid "All available (default)"
msgstr "Kaikki saatavilla (oletus)"

#: src/vpn-editor/Pptp/Advanced.qml:111
msgid "128-bit (most secure)"
msgstr "128-bit (turvallisin)"

#: src/vpn-editor/Pptp/Advanced.qml:112
msgid "40-bit (less secure)"
msgstr "40-bit (vähemmän turvallinen)"

#: src/vpn-editor/Pptp/Advanced.qml:125
msgid "Allow stateful encryption"
msgstr "Salli tilallinen salaus"

#: src/vpn-editor/Pptp/Advanced.qml:128
msgid "Compression"
msgstr "Pakkaus"

#: src/vpn-editor/Pptp/Advanced.qml:136
msgid "Allow BSD data compression"
msgstr "Salli BSD-datapakkaus"

#: src/vpn-editor/Pptp/Advanced.qml:145
msgid "Allow Deflate data compression"
msgstr "Salli Deflate-datapakkaus"

#: src/vpn-editor/Pptp/Advanced.qml:154
msgid "Use TCP header compression"
msgstr "Käytä TCP-otsakkeen pakkausta"

#: src/vpn-editor/Pptp/Advanced.qml:157
msgid "Echo"
msgstr ""

#: src/vpn-editor/Pptp/Advanced.qml:165
msgid "Send PPP echo packets"
msgstr "Lähetä PPP echo -paketteja"

#: src/vpn-editor/Pptp/Editor.qml:54
msgid "Gateway:"
msgstr "Yhdyskäytävä:"

#: src/vpn-editor/Pptp/Editor.qml:57
msgid "Optional"
msgstr "Valinnainen"

#: src/vpn-editor/Pptp/Editor.qml:66
msgid "User name:"
msgstr "Käyttäjätunnus:"

#: src/vpn-editor/Pptp/Editor.qml:86
msgid "NT Domain:"
msgstr ""

#: src/vpn-editor/VpnEditor.qml:25
#, qt-format
msgid "Editing: %1"
msgstr "Muokataan: %1"

#: src/vpn-editor/VpnList.qml:26
msgid "VPN configurations"
msgstr "VPN-määritykset"

#: src/vpn-editor/VpnList.qml:40
msgid "OpenVPN"
msgstr "OpenVPN"

#: src/vpn-editor/VpnList.qml:45
msgid "PPTP"
msgstr "PPTP"

#: src/vpn-editor/VpnList.qml:86
msgid "Delete configuration"
msgstr "Poista määritys"

#: src/vpn-editor/VpnList.qml:100
msgid "No VPN connections"
msgstr "Ei VPN-yhteyksiä"
